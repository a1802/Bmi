package com.tom.bmi;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/2/25.
 */

public class Person implements Serializable{
    String name;
    float weight;
    float height;

    public Person(String name, float weight, float height) {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    public Person(float weight, float height){
        this.weight = weight;
        this.height = height;
    }

    public float bmi(){
        float bmi = weight/(height*height);
        return bmi;
    }
}
