package com.tom.bmi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        TextView result = findViewById(R.id.result);
        Person p = (Person) getIntent()
                .getSerializableExtra(getString(R.string.key_person));
        float bmi = p.bmi();
                /*getIntent()
                .getFloatExtra("EXTRA_BMI", 0);*/
        result.setText(String.valueOf(bmi));
    }
}
