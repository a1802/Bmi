package com.tom.bmi;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edWeight;
    private EditText edHeight;
    private TextView result;
    private Button bHelp;
    private EditText edName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AA", "onCreate");
        setContentView(R.layout.activity_main);
        findViews();
        final int numOfAttendences = 10;
        bHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("介紹BMI")
                        .setPositiveButton("OK", null)
                        .show();
            }
        });
        edHeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("TEXT", charSequence.toString());
                Toast.makeText(MainActivity.this,
                        charSequence,
                        Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void findViews() {
        edName = findViewById(R.id.ed_name);
        edWeight = findViewById(R.id.ed_weight);
        edHeight = findViewById(R.id.ed_height);
        result = findViewById(R.id.result);
        bHelp = findViewById(R.id.b_help);
    }

    public void bmi(View v){
        String name = edName.getText().toString();
        String w = edWeight.getText().toString();
        String h = edHeight.getText().toString();
        float weight = Float.parseFloat(w);
        float height = Float.parseFloat(h);
        Person p = new Person(name, weight, height);
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(getString(R.string.key_person), p);
        startActivity(intent);
//        float weight = Float.parseFloat(edWeight.getText().toString());
//        old();

        /*new AlertDialog.Builder(this)
                .setTitle("BMI")
                .setMessage("your bmi is " + bmi)
                .setPositiveButton("OK", null)
                .show();*/
        //
        /*AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("BMI");
        builder.setMessage("your bmi is " + bmi);
        builder.setPositiveButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.show();*/


    }

    private void old() {
        String w = edWeight.getText().toString();
        String h = edHeight.getText().toString();
        float weight = Float.parseFloat(w);
        float height = Float.parseFloat(h);
        float bmi = weight/(height* height);
        Log.d("KK", bmi + "");
        Toast.makeText(this,
                "Your BMI is "+bmi,
                Toast.LENGTH_LONG).show();
        result.setText(bmi+"");

        Intent intent = new Intent(
                this, ResultActivity.class);
        intent.putExtra("EXTRA_BMI", bmi);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("AA", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("AA", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("AA", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("AA", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AA", "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("AA", "onRestart");
    }
}





